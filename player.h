#ifndef __PLAYER_H__
#define __PLAYER_H__

#include <iostream>
#include "board.h"
using namespace std;

class Player{

public:
    Player(Side side);
    ~Player();
    Side P_side;
    Side other;
    Board *P_board;
    
    Move *doMove(Move *opponentsMove, int msLeft);
	int value(Move *move);
	
    // Flag to tell if the player is running within the test_minimax context
    bool testingMinimax;
};

#endif
