
Arpit and Jared worked together on mostly everything that had to do with the assignments over the last two weeks. 
Any changes we made were done in each others presence, and often we worked on the same computer, brainstorming together before 
making any changes. Furthoremore, last week we both attended office hours together. This week, we both worked on attempting to 
improve our AI some more, but neither of us were successful in increasing our AIs ability. 

At the very beginning we had an AI capable of placing random pieces on the board. Following this, we implemented a simple heuristic 
which increased the value of moves depending if they were edge values, corner values, or gave access to an edge or corner. Then, 
in an attempt to make it better we tried to implement the minimax algorithm to look a few steps in the future, but we were 
incapable of correctly implementing it. Following this, we attempted to make our heuristic more complex, but it actually made the
 AI beat simple player less, so rather than use this new heuristic, we concluded it was safest to just use our orignal one.
 Although this is not the greatest strategy, it is what we have and we are confident that it will beat other players who do not 
have an algorithm that looks into the future.