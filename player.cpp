#include "player.h"
#include <stdio.h>

using namespace std;

/*
 * Constructor for the player; initialize everything here. The side your AI is
 * on (BLACK or WHITE) is passed in as "side". The constructor must finish 
 * within 30 seconds.

*/

Player::Player(Side side) {
    // Will be set to true in test_minimax.cpp.
    testingMinimax = false;
       
     P_side = side;
	 other = (side == BLACK) ? WHITE : BLACK;
	 P_board = new Board();

}

/*
 * Destructor for the player.
 */
Player::~Player() {

}

/*
 * Compute the next move given the opponent's last move. Your AI is
 * expected to keep track of the board on its own. If this is the first move,
 * or if the opponent passed on the last move, then opponentsMove will be NULL.
 *
 * msLeft represents the time your AI has left for the total game, in
 * milliseconds. doMove() must take no longer than msLeft, or your AI will
 * be disqualified! An msLeft value of -1 indicates no time limit.
 *
 * The move returned must be legal; if there are no valid moves for your side,
 * return NULL.
 */
Move *Player::doMove(Move *opponentsMove, int msLeft) {
	
	Move *optimal = new Move(0, 0);
	optimal = NULL;
	
	//Updates board with opponents move if they did move
	if (opponentsMove != NULL)
	{
		P_board->doMove(opponentsMove, other);
	}
	
	//Iterates through all possible tiles
	if (P_board->hasMoves(P_side))
	{
		for (int i = 0; i < 8; i++) 
		{
			for (int j = 0; j < 8; j++) 
			{
				Move *move = new Move(i,j);
				//If a certain move is possible
				if (P_board->checkMove(move, P_side)) 
				{
					//Check its value using helper function "value"
					//and if it is the highest remember it
					if (value(move) > value(optimal))
					{
						optimal = move;
					}
				}
			}
		}
		//Do the highest value move and update the board with it
		P_board->doMove(optimal, P_side);
		return optimal;
	}

    return NULL;
}


/*
 * Compute the value of making a move based on the current board 
 * position.  Takes a move as an argument.
 */
int Player::value(Move *move)
{
	//Initial case
	if (move == NULL)
	{
		return 0;
	}
	int x = move->getX();
	int y = move->getY();
	
	Board *test = P_board->copy();
	test->doMove(move, P_side);
	
	//Moves that are corners are weighted times 3
	int val = test->count(P_side);
	
	if ((x == 0 && y == 0) || (x == 0 && y == 7) || (x == 7 && y == 7) || (x == 7 && y == 0))
	{
		val *= 3;
	} 
	
	//Moves that are edges are weighted times 1.5
	else if (x == 0 || x == 7 || y == 0 || y == 7)
	{
		val *= 1.5;
	}
	
	//Moves that give access to an edge are weighted times .5
	else if (x == 1 || x == 6 || y == 1 || y == 6)
	{
		val *= .5;
	}
	
	//Moves that give access to a corner are weighted times -1
	else if ((x == 1 && y == 1) || (x == 1 && y == 6) || (x == 6 && y == 1) || (x == 6 && y == 6))
	{
		val *= -1;
	}
	
	return val;
}
